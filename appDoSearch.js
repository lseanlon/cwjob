var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');


var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};
//read main file for reference
// read recordFinalDetails reference file
// parse json
// loop entire folder, read entire index file
// download google result for each keyword

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

var listOfLink = [];
var listOfRow = [];
//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];


require('shelljs/global');

// Sync call to exec()
var version = exec('node --version', { silent: true }).output;


//download all the first page listing
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var ref = urlInfo[2];

    var jsonFileDetails = fs.readFileSync('./dist/recordFinalDetails-' + ref + '.txt', 'utf8');
    jsonFileDetails = JSON.parse(jsonFileDetails);

    var listRef = jsonFileDetails;
    var leng = listRef.length;
    //test: leng = 100;
    for (var i = 0; i <= leng; i++) {
        if (listRef[i]) {
            //console.log(listRef[i]);
            if (!listRef[i].jobId) {
                listRef[i].jobId = "i" + i;
            }
            downloadPage(i, 1, listRef[i], timerWait, (i + 1) == listRef.length);
        }

    }



});

function run_cmd(cmd, args, cb, end) {
    var spawn = require('child_process').spawn,
        child = spawn(cmd, args),
        me = this;
    this.stdout = "";
    child.stdout.on('data', function(buffer) { cb(me, buffer) });
    child.stdout.on('end', end);
}



function downloadPage(i, j, _elem, timerWait, _isLastIndex) {

    var indextime = i ? i : 1;
    var indextimej = j * 3000;
    timerWait = (indextime * 10650 + indextimej);

    setTimeout(function() {
        console.log('downloadPage');
        var keyword = _elem["keyword" + j];

        console.log('index keyword', "keyword" + j);
        if (!_elem || !keyword) {
            return;
        }


        var urlLinkPage = 'google.co.uk/?q=' + keyword + '&oq=ds&aqs=chrome..09i57l2j69i60l4.722j0j4&sourceid=chrome&ie=UTF-8';
        urlLinkPage = escape(urlLinkPage);
        var outputfolderpath = "./dist/FinalSearchPage/" + _elem.ref + "/";
        var outputfilepath = outputfolderpath + i + keyword.replace(/[^a-z0-9]/gi, '_').toLowerCase() + ".txt";


        mkdirp("./dist/FinalSearchPage/", function(err) {
            if (err) return cb(err);
            console.log("create folder path", "./dist/FinalSearchPage/");
        });
        mkdirp(outputfolderpath, function(err) {
            if (err) return cb(err);
            console.log("create folder path", outputfolderpath);
        });

        downloadPageForKeyword(_elem, i, 'keyword' + j, urlLinkPage, outputfolderpath, outputfilepath);



        _isLastIndex = true;
        if (_isLastIndex) {}
        var pathname = "./dist/downloaded-" + _elem.ref + "SearchData.txt"
        console.log("Write download result to path ", pathname);
        fs.writeFileSync(pathname, JSON.stringify(listOfLink));



        //repeat for different index
        if (j < 3) { downloadPage(i, j + 1, _elem, timerWait, _isLastIndex); }

    }, timerWait);
}

function downloadPageForKeyword(_elem, i, _keyword, urlLinkPage, outputfolderpath, outputfilepath) {


    var extraTimer = 10650;




    if (fs.existsSync(outputfilepath)) { extraTimer = 0; }
    if (outputfilepath.length > 50) {
        outputfilepath = outputfilepath.substring(0, 49) + ".txt";
    }

    console.log('outputfilepath:', outputfilepath);
    console.log('downloadPageForKeyword');

    console.log("start downloading ", urlLinkPage);
    if (fs.existsSync(outputfilepath)) {
        console.log("File downloaded. Already found. Skipped downloading ", urlLinkPage);
        console.log("File downloaded. Skipped downloading path ", outputfilepath);

    } else {

        var keywordtext = _elem[_keyword];
        keywordtext = keywordtext.replace(/  /gi, ' ');

        //very long keywords- split out words
        var keywordList = keywordtext.split(" ");
        if (keywordList.length > 15) {
            keywordList = keywordList.slice(0, 15);
            keywordtext = keywordList.join(" ");
        }

        if (keywordtext) {
            keywordtext = keywordtext.replace(/\n/gi, ' ');
            keywordtext = keywordtext.replace(/\r/gi, ' ');
            exec('sleep 3.2s &&  phantomjs GoogleSearch.js "' + (keywordtext) + '"  && sleep 1.54s ', function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                try {
                    fs.writeFileSync(outputfilepath, output);
                    fs.writeFileSync(outputfilepath + ".html", output);
                    console.log('done download - ' + urlLinkPage + ' into ' + outputfilepath);
                    _elem['isDownloaded'] = true;
                } catch (e) {
                    console.log('err download - ' + urlLinkPage + ' into ' + outputfilepath);
                    console.log(e);

                    _elem['isDownloaded'] = false;
                }


            });

        }

    }

    _elem['urlLinkPage' + _keyword] = urlLinkPage;
    _elem['outputfilepath-' + _keyword] = outputfilepath;



    //remove old similar - prevent same id pushed twice
    var newList = [];
    for (var x = 0; x < listOfLink.length; x++) {
        if (listOfLink[x].jobId != _elem.jobId) {
            newList.push(listOfLink[x]);
        }

    }
    listOfLink = newList;
    listOfLink.push(_elem);


}
