var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');

var jsonListIndexStr="[" + fs.readFileSync("./dist/listingIndex.txt", 'utf8')  +"{}]";
var jsonIndexList=JSON.parse(jsonListIndexStr);

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
  

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

//get config
//loop config, 
// loop indexes

 var listOfLink =  {}

var downloadedEntryPathList = [];
//download all the first page listing
jsonIndexList.forEach(function(_elem, index, collection) {
	if(!_elem){
		return ; 
	} 
	
    var randomNum = Math.floor(Math.random() * 574) + 1;
 

	if(!_elem ||!_elem.numberOfPage){
		return ; 
	} 

     for (var i = 1; i <= _elem.numberOfPage; i++) {  

       addAllLinkInformation(i, _elem, timerWait + randomNum,i == _elem.numberOfPage) ; 
 		 
    }


});



 

function addAllLinkInformation(i, _elem, timerWait,_isLastIndex) {
 // grab file, cheerio
// grab link html

   // setTimeout(function() { 
    
		var fileContent = "";
		var filefullnamepath= _elem.filePathFormat + i +".txt";
		try{ 
		 fileContent = fs.readFileSync(filefullnamepath, 'utf8');
		}catch(e){ 
			console.log('SKIPPED READ ,BECAUSE DOWNLOAD FAILED FOR ' , filefullnamepath);
			return ;
		}
 	 	var $ = cheerio.load(fileContent)
	 
 
 	 	 var temList=[]; 
        $('.job-title').find('a').each(function(i, element) { 
 
		 var curLink  ='https://www.cwjobs.co.uk'+ $(this).prop('href')  ; 
    	var curRow = { "link": curLink, "ref" :_elem.ref ? _elem.ref:""};

 	 	 if(!listOfLink[ _elem.ref]){ listOfLink[ _elem.ref] = [];}
 	 	 listOfLink[ _elem.ref].push(curRow);



		
		});

	   
	   if(_isLastIndex){ 
	   	console.log('writee' ,  listOfLink);
	   	 fs.writeFileSync(  "./dist/detailLinkIndex.txt", JSON.stringify(listOfLink)); 
	   }

   // }, i * 3000);
}



 
