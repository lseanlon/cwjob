#download  all the first listing page, then download all its listing until next is disabled.. 
 
rm -rf dist/listingIndex.txt
node appDownloadEntryPage.js ./src/configuration/UrlList.txt   


#extract information from first page on the details link
rm -rf ./dist/detailLinkIndex.txt
node appGetLinkDetailsPage.js 


#download all the details link 
rm -rf ./dist/downloadedFinalDetails*.txt
node appDownloadDetailsPage.js ./src/configuration/UrlList.txt    
 

#read all the details link ,  grab out details
rm -rf ./dist/recordFinalDetails*.txt
rm -rf ./dist/*data.xlsx
node appReadDetailsPage.js ./src/configuration/UrlList.txt    