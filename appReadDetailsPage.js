var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');


var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

// read downloadedFinalDetails file
// parse json
// loop entire folder, read entire index file
// scrap out all row into json
// save into output file

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

var striphtml = function(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
var chunks = require('chunk-array').chunks
var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var ref = urlInfo[2];
    var jsonFileDetails;

    var filefullnamepath= './dist/downloadedFinalDetails-' + ref + '.txt'
    try { 
        jsonFileDetails = fs.readFileSync(filefullnamepath, 'utf8');
        jsonFileDetails = JSON.parse(jsonFileDetails);
    } catch (e) {
        console.log('SKIPPED READ BECAUSE DOWNLOAD FAILED FOR ', filefullnamepath);
        return;
    }


    var listRef = jsonFileDetails[ref];
    for (var i = 1; i <= listRef.length; i++) {
        if (listRef[i]) {


            var elem = listRef[i];
            console.log("Reading file ", elem["outputfilepath"]);
            var filehtml = fs.readFileSync(elem["outputfilepath"], 'utf8');
            var $ = cheerio.load(filehtml);

            var currentRow = {};
            currentRow['originalLink'] = elem["urlLinkPage"];


            $('.contact-reference .reference').remove()

            //contactName
            var spanHtml = $('.contact-reference').find('li').eq(0).find('span').prop('outerHTML')
            var contactName = $('.contact-reference').find('li').eq(0).html();
            contactName = contactName.replace(spanHtml, '');
            currentRow['contactName'] = contactName;


            //refencejob
            var spanHtml = $('.contact-reference').find('li').eq(1).find('span').prop('outerHTML')
            var refenceJob = $('.contact-reference').find('li').eq(1).html();
            refenceJob = refenceJob.replace(spanHtml, '');
            currentRow['refenceJob'] = refenceJob;


            //jobid
            var spanHtml = $('.contact-reference').find('li').eq(2).find('span').prop('outerHTML')
            var jobId = $('.contact-reference').find('li').eq(2).html();
            jobId = jobId.replace(spanHtml, '');
            currentRow['jobId'] = jobId;

            //description
            $('.job-description').remove('.contact-reference');
            var jobDesc = $('.job-description p').eq(0).html()
            currentRow['rawJobDesc'] = jobDesc;
            currentRow['jobDesc'] = $('.job-description p').eq(0).text();

            //experienceRequirements 
            currentRow['experienceRequirements'] = $("[property='experienceRequirements']").html();
            currentRow['experienceRequirements'] = $("[property='experienceRequirements']").text();

            currentRow['experienceRequirements'] = currentRow['experienceRequirements'] ? currentRow['experienceRequirements'] : "/ / / / / / / /";
            var requirements = currentRow['experienceRequirements'].split(" ");
            // var groupLength = Math.floor(requirements.length / 3);
            // var results = chunks(requirements, 3); 
            //chosenkeyword 
            //find <br/> * 


            requirements = requirements.filter(Boolean)
                // currentRow['keyword1'] = requirements[0] + " " + requirements[1] + requirements[2];
                // currentRow['keyword2'] = requirements[3] + " " + requirements[4] + requirements[5];
                // currentRow['keyword3'] = requirements[6] + " " + requirements[7] + requirements[8];
            currentRow['keyword1'] = '';
            currentRow['keyword2'] = '';
            currentRow['keyword3'] = '';

            //filter out bullet lists
            var BulletList = []
            $('.job-description :not(".contact-reference")').find('li').each(function(i, element) {
                BulletList.push($(element).html());
            });
            if (!jobDesc) { jobDesc = ''; }

            while (jobDesc.indexOf("*") != -1) {
                jobDesc = jobDesc.replace("*", "\[asterisk\]");
            }
            if (BulletList.length) {
                if (BulletList[0]) {
                    currentRow['keyword1'] += '  '
                    currentRow['keyword1'] += BulletList[0]
                }
                if (BulletList[1]) {
                    currentRow['keyword1'] += '  '
                    currentRow['keyword1'] += BulletList[1]
                }
                if (BulletList[2]) {
                    currentRow['keyword2'] += '  '
                    currentRow['keyword2'] += BulletList[2]
                }
                if (BulletList[3]) {
                    currentRow['keyword2'] += '  '
                    currentRow['keyword2'] += BulletList[3]
                }
                if (BulletList[4]) {
                    currentRow['keyword3'] += '  '
                    currentRow['keyword3'] += BulletList[4]
                }
                if (BulletList[5]) {
                    currentRow['keyword3'] += '  '
                    currentRow['keyword3'] += BulletList[5]
                }
            } else {

                jobDesc = jobDesc.replace(/\<br\>o /gi, '[listbullet]')
                jobDesc = jobDesc.replace(/\<br\>·/gi, '[listbullet]')
                jobDesc = jobDesc.replace(/\<br\>- /gi, '[listbullet]')
                    // jobDesc=jobDesc.replace(/\<br\>· /gi,'[listbullet]') 
                jobDesc = jobDesc.replace(/\[asterisk\]/gi, '[listbullet]')


                //filter out Dash lists
                // 
                var DashList = jobDesc.split('[listbullet]')


                var sanitizeDash = function(_val) {
                    //take out all html   
                    return ($("<p>" + _val + "</p>").text());
                }
                DashList = DashList.map(sanitizeDash);

                if (DashList.length) {
                    if (DashList[0]) {
                        currentRow['keyword1'] += '  '
                        currentRow['keyword1'] += DashList[0]
                    }
                    if (DashList[1]) {
                        currentRow['keyword1'] += '  '
                        currentRow['keyword1'] += DashList[1]
                    }
                    if (DashList[2]) {
                        currentRow['keyword1'] += '  '
                        currentRow['keyword1'] += DashList[2]
                    }
                    if (DashList[3]) {
                        currentRow['keyword2'] += '  '
                        currentRow['keyword2'] += DashList[3]
                    }
                    if (DashList[4]) {
                        currentRow['keyword2'] += '  '
                        currentRow['keyword2'] += DashList[4]
                    }
                    if (DashList[5]) {
                        currentRow['keyword2'] += '  '
                        currentRow['keyword2'] += DashList[5]
                    }
                    if (DashList[6]) {
                        currentRow['keyword3'] += '  '
                        currentRow['keyword3'] += DashList[6]
                    }
                    if (DashList[7]) {
                        currentRow['keyword3'] += '  '
                        currentRow['keyword3'] += DashList[7]
                    }
                    if (DashList[8]) {
                        currentRow['keyword3'] += '  '
                        currentRow['keyword3'] += DashList[8]
                    }
                }

            }





            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.replace(new RegExp(search, 'g'), replacement);
            };
            currentRow['keyword1'] = currentRow['keyword1'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')
            currentRow['keyword2'] = currentRow['keyword2'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')
            currentRow['keyword3'] = currentRow['keyword3'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')

            currentRow['keyword1'] = ($("<p>" + currentRow['keyword1'] + "</p>").text())
            currentRow['keyword2'] = ($("<p>" + currentRow['keyword2'] + "</p>").text())
            currentRow['keyword3'] = ($("<p>" + currentRow['keyword3'] + "</p>").text())


            //area
            var local = $("meta[property='addressLocality']").prop('content') ? $("meta[property='addressLocality']").prop('content') : "";
            var regionn = $("meta[property='addressRegion']").prop('content') ? $("meta[property='addressRegion']").prop('content') : ""
            var area = local + "," + regionn
            currentRow['area'] = area;

            //salary
            var salary = $("[property='baseSalary']").html();
            currentRow['salary'] = unescape(salary);
            currentRow['salary'] = currentRow['salary'].replace('&#xA3;', 'Pound').replace('&#xA3;', 'Pound');

            //company
            var company = $("[property='hiringOrganization']").find('a').html();
            currentRow['company'] = company;

            //emptype - contract -etc
            employmentType = $("[property='employmentType']").html();
            currentRow['employmentType'] = employmentType;

            //  title
            var meta = $("[property='title']").find('meta').prop('outerHTML')
                // $("[property='title']").remove('meta');
            $("[property='title']").find('meta').remove();
            title = $("[property='title']").html();
            title = title.replace(meta, '');
            currentRow['title'] = title;

            //companyurl
            companyUrl = $("[property='url']").prop('content');
            currentRow['companyUrl'] = companyUrl;
            currentRow['ref'] = ref;



            console.log("currentRow ", currentRow['title']);
            if (!finalResultExcelList[ref]) { finalResultExcelList[ref] = []; }
            finalResultList.push(currentRow);
            finalResultExcelList[ref].push(currentRow);
        }


    }

    if (urlList.length == (index + 1)) {
        var filepath = './dist/recordFinalDetails-' + ref + '.txt'
        console.log("Write to file ", filepath);
        fs.writeFileSync(filepath, JSON.stringify(finalResultList));
    }

});




//write into excel 
var json2xls = require('json2xls');
for (var key in finalResultExcelList) {
    if (finalResultExcelList.hasOwnProperty(key)) {
        // console.log(key + " -> " + finalResultExcelList[key]);  
        var xls = json2xls(finalResultExcelList[key]);
        var outexcelpath = './dist/' + key + '-data.xlsx';
        console.log("Write to excel ", outexcelpath);
        fs.writeFileSync(outexcelpath, xls, 'binary');
    }
}
