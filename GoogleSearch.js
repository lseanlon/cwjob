#!/usr/bin/env phantomjs

//--------------------------------------------------------

var page = require('webpage').create(),
    system = require('system'),
    action = null,
    q = null;

//--------------------------------------------------------

if (system.args.length === 1) {
    //-console.log('Usage: google.js <some Query>');
    phantom.exit(1);
} else {
    q = system.args[1];
}

//--------------------------------------------------------

start = function() {
    //-console.log('ACTION: start');
    page.evaluate(function(q) {
        if (q) { 
            console.log("Begin search for val",q);
            $('input[name="q"]').val(q);
            $('form[action="/search"]').submit();
        } else {
            console.log("blank");
           // phantom.exit();
           // return  ; 
        }
    }, q);
    page.render('./start.png');
    action = viewList;
}

viewList = function() {
    //-console.log('ACTION: viewList');
    // page.render('./viewList.png');
    page.evaluate(function() {
        console.log($('body').html());
    });
    phantom.exit();
}

//--------------------------------------------------------

work = function() {
    if (action == null) action = start;
    ////-console.log( "URL: " + page.url );
    action.call();
}

injectJQuery = function(callback) {
    //  //-console.log('injecting JQuery');
    page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js", callback);
}

page.onLoadFinished = function(status) {
    //  //-console.log('Status: ' + status);
    if (status == 'success') {
        injectJQuery(work);
        //-console.log('test');
    } else {
        //-console.log('Connection failed.');
        phantom.exit();
    }
}

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onResourceReceived = function(response) {
    //if(response.stage == "end")
    //-console.log('Response (#' + response.id + ', status ' + response.status + '"): ' + response.url);
}

page.onUrlChanged = function(url) {
    //-console.log("URL: " + url);
}

//--------------------------------------------------------

page.open('http://www.google.co.uk');
