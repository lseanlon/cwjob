var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {
    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];


    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }


    mkdirp("./dist/", function(err) {
        if (err) return cb(err);
        console.log("create folder path", "./dist/");
    });



    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        if (fs.existsSync(urlPath)) {
            console.log('File downloaded found.Skipping', urlPath);
        } else {}

        console.log('start download ', urlLink);
        var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };

        download(urlLink, downloadPath).then((data) => {
            console.log('done download ', urlLink);
            fs.writeFileSync(urlPath, data);
            if (!urlPath) {
                return;
            }

            handleSubPages(curRow);

        });

        downloadedEntryPathList.push(curRow);

    }, index * timerWait + randomNum);

});

var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;


    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');
    var $ = cheerio.load(fileContent);
    //determine number of record
    var totalrec = $('.page-title span').html();
    totalrec = totalrec.replace(/,/g, "");
    totalrec = parseFloat(totalrec);

    // determine record per page
    var listingPerPage = $('.job-title').length;
    listingPerPage = 20;

    console.log('totalrec', totalrec);
    console.log('listingPerPage', listingPerPage);

    //divide no of record over pagination, use ceiling
    var numberOfPage = Math.ceil(totalrec / listingPerPage);

    console.log('numberOfPage', numberOfPage);

    var filePathFormat = _elem.downloadPath + "pages-" + _elem.ref;
    outputJson = { "numberOfPage": numberOfPage, "ref": _elem.ref, "filePathFormat": filePathFormat };
    fs.appendFileSync('./dist/listingIndex.txt', JSON.stringify(outputJson) + ",");

    // //download all the page 
    for (var i = 1; i <= numberOfPage; i++) {
        downloadAllListPages(i, _elem, timerWait + randomNum)
    }



};

function downloadAllListPages(i, _elem, timerWait) {
    var urlLinkPage = "";

    //already has param string
    if (_elem.urlLink && (_elem.urlLink.indexOf("?") > -1 || _elem.urlLink.indexOf("&") > -1)) {
        urlLinkPage = _elem.urlLink + "&page=" + i;
    } else {
        urlLinkPage = _elem.urlLink + "?page=" + i;
    } 
    
    var urlLinkDownloadPage = _elem.downloadPath + "pages-" + _elem.ref + i + ".txt";

    mkdirp(_elem.downloadPath, function(err) {
        if (err) return cb(err);
        console.log("create folder path", _elem.downloadPath);
    });


    var newnum = 0;
    if (fs.existsSync(urlLinkDownloadPage)) {
        newnum = -3000;
    }

    setTimeout(function() {
        if (fs.existsSync(urlLinkDownloadPage)) {
            console.log('File downloaded found. Skipping download', urlLinkPage);
        } else {
            console.log('urlLinkDownloadPage', urlLinkDownloadPage);
            console.log('urlLinkPage', urlLinkPage);
            console.log(' _elem.downloadPath', _elem.downloadPath);


            download(urlLinkPage, _elem.downloadPath).then((data) => {
                fs.writeFileSync(urlLinkDownloadPage, data);
                console.log('done download - ' + _elem.urlLink + ' into ' + _elem.urlPath);
            });
        }
    }, i * 3000 - newnum);
}
